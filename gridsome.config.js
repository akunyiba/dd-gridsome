const path = require('path')

// const axios = require('axios')

// module.exports = function (api) {
//   api.loadSource(async store => {
//     const { data } = await axios.get('http://dd-test.loc:8888/wp-json/menus/v1/menus/main')

//     const contentType = store.addContentType({
//       typeName: 'Menus',
//       route: 'menus/:id'  // add this for one dynamic route...
//     })

//     for (const item of data) {
//       contentType.addNode({
//         id: item.id,
//         title: item.title
//         path: `menus/${item.id}` //... or this for a route per item
//       })
//     }
//   })
// }

function addStyleResource (rule) {
  rule.use('style-resource')
    .loader('style-resources-loader')
    .options({
      patterns: [
        path.resolve(__dirname, './src/assets/sass/_globals.sass'),
        // or if you use scss
        path.resolve(__dirname, './src/assets/sass/_globals.scss'),
        // you can also use a glob if you'd prefer
        path.resolve(__dirname, './src/assets/sass/*.sass'),
        // or scss
        path.resolve(__dirname, './src/assets/sass/*.scss'),
      ],
    })
}

module.exports = {
  runtimeCompiler: true,
  chainWebpack (config) {
    // Load variables for all vue-files
    const types = ['vue-modules', 'vue', 'normal-modules', 'normal']
    
    types.forEach(type => {
      addStyleResource(config.module.rule('sass').oneOf(type))
    })

    // or if you use scss
    types.forEach(type => {
      addStyleResource(config.module.rule('scss').oneOf(type))
    })
  },
  icon: {
    favicon: './src/favicon.png',
    touchicon: './src/favicon.png'
  },
  siteName: 'Gridsome',
  siteDescription: 'A WordPress starter for Gridsome',
  plugins: [
    {
      use: '@gridsome/source-wordpress',
      options: {
        baseUrl: 'http://dd-test.loc:8888/', // required
        apiBase: 'wp-json',
        typeName: 'WordPress', // GraphQL schema name (Optional)
        perPage: 3, // How many posts to load from server per request (Optional)
        concurrent: 10, // How many requests to run simultaneously (Optional)
        routes: {
          post: '/:year/:month/:day/:slug', //adds route for "post" post type (Optional)
          post_tag: '/tag/:slug' // adds route for "post_tag" post type (Optional)
        }
      }
    }

  ]
}
